<?php

namespace Drupal\jsonapi_push\EventSubscriber;

use Drupal\jsonapi\EventSubscriber\ResourceResponseSubscriber as JsonapiResourceResponseSubscriber;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ResourceResponseSubscriber.
 *
 * @internal
 */
final class ResourceResponseSubscriber extends JsonapiResourceResponseSubscriber {

  /**
   * {@inheritdoc}
   *
   * If the request has the `serverPush` query parameter, all fields are
   * omitted. This leaves the normalized resource object with only a type, id
   * and links object, which will be used for a subsequent request that will
   * receive the complete, pushed resource (or one with the requested sparse
   * fieldset).
   */
  protected static function generateContext(Request $request) {
    $context = parent::generateContext($request);
    if ($request->query->has('serverPush')) {
      $context['sparse_fieldset'] = [];
      /* @var \Drupal\jsonapi\ResourceType\ResourceType $resource_type */
      foreach (\Drupal::service('jsonapi.resource_type.repository')->all() as $resource_type) {
        $context['sparse_fieldset'][$resource_type->getTypeName()] = [''];
      }
    }
    return $context;
  }

}
