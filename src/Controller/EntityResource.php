<?php

namespace Drupal\jsonapi_push\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\jsonapi\Controller\EntityResource as JsonapiEntityResource;
use Drupal\jsonapi\JsonApiResource\Data;
use Drupal\jsonapi\JsonApiResource\IncludedData;
use Drupal\jsonapi\JsonApiResource\Link;
use Drupal\jsonapi\JsonApiResource\LinkCollection;
use Drupal\jsonapi\JsonApiResource\NullIncludedData;
use Drupal\jsonapi\JsonApiResource\ResourceObject;
use Drupal\jsonapi\JsonApiResource\ResourceObjectData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class EntityResource.
 *
 * @internal
 */
final class EntityResource extends JsonapiEntityResource {

  /**
   * {@inheritdoc}
   */
  protected function buildWrappedResponse($data, Request $request, IncludedData $includes, $response_code = 200, array $headers = [], LinkCollection $links = NULL, array $meta = []) {
    if ($request->query->has('serverPush') && $this->user->isAnonymous()) {
      $data = $this->toPushedData($data, $request);
      $includes = $this->toPushedData($includes, $request);
      $get_self_link = function (ResourceObject $resource_object) {
        return iterator_to_array($resource_object->getLinks())['self'][0];
      };
      $push_links = array_merge(
        array_map($get_self_link, iterator_to_array($data)),
        array_map($get_self_link, iterator_to_array($includes)),
      );
    }
    $response = parent::buildWrappedResponse($data, $request, $includes, $response_code, $headers, $links, $meta);
    if (!empty($push_links)) {
      $this->pushLinks($response, $push_links);
    }
    $response->addCacheableDependency((new CacheableMetadata())->addCacheContexts([
      'url.query_args:serverPush',
    ]));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getIncludes(Request $request, $data) {
    assert($data instanceof ResourceObject || $data instanceof ResourceObjectData);
    if ($request->query->has('serverPush') && ($parameter_value = $request->query->get('serverPush')) && !empty($parameter_value)) {
      if ($request->query->has('include')) {
        throw new BadRequestHttpException('The `serverPush` query parameter is incompatible with the `include` query parameter.');
      }
      $request->query->set('include', $parameter_value);
      $includes = parent::getIncludes($request, $data);
      $request->query->remove('include');
    }
    else {
      $includes = parent::getIncludes($request, $data);
    }
    return $includes;
  }

  /**
   * Maps input to a set of resource object with self links suitable for push.
   *
   * @param \Drupal\jsonapi\JsonApiResource\Data $data
   *   The response data for which the self link should be modified for a push
   *   capable response.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The initiating request object.
   *
   * @return \Drupal\jsonapi\JsonApiResource\Data|\Drupal\jsonapi\JsonApiResource\IncludedData|\Drupal\jsonapi\JsonApiResource\ResourceObjectData
   *   The input data, with updated self links.
   */
  protected function toPushedData(Data $data, Request $request) {
    if (!$data instanceof ResourceObjectData || $data instanceof NullIncludedData) {
      return $data;
    }
    $pushed = [];
    foreach ($data as $resource_object) {
      assert($resource_object instanceof ResourceObject);
      $original_links = $resource_object->getLinks();
      $self_link = NULL;
      $links = $original_links->filter(function ($key, $links) use (&$self_link) {
        if ($key === 'self') {
          $self_link = reset($links);
          return FALSE;
        }
        return TRUE;
      });
      assert($self_link instanceof Link);
      $self_uri = $self_link->getUri();
      $original_query = $self_uri->getOption('query') ?: [];
      $filtered_query = array_filter($original_query, function ($param_name) {
        $excluded = ['fields', 'serverPush'];
        return !in_array($param_name, $excluded, TRUE);
      }, ARRAY_FILTER_USE_KEY);
      $resource_type = $resource_object->getResourceType();
      $fields = array_intersect_key(
        $request->query->get('fields') ?: [],
        array_flip([$resource_type->getTypeName()])
      );
      $pushed_url = $self_uri->setOption('query', array_merge($filtered_query, !empty($fields) ? ['fields' => $fields] : []));
      $overridden_link = new Link(CacheableMetadata::createFromObject($self_link), $pushed_url, $self_link->getLinkRelationTypes(), $self_link->getTargetAttributes());
      $pushed[] = new ResourceObject(
        $resource_object,
        $resource_type,
        $resource_object->getId(),
        $resource_type->isVersionable() ? explode(':', $resource_object->getVersionIdentifier())[1] : NULL,
        $resource_object->getFields(),
        $links->withLink('self', $overridden_link)
      );
    }
    return $data instanceof IncludedData ? new IncludedData($pushed) : new ResourceObjectData($pushed);
  }

  /**
   * Adds the given links as rel=preload headers to the given response.
   *
   * The will initiate a push on common HTTP/2 enabled servers and a browser
   * preload otherwise.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response for which links should be pushed.
   * @param \Drupal\jsonapi\JsonApiResource\Link[] $links
   *   The links to push.
   */
  protected function pushLinks(Response $response, array $links) {
    $format = '<%s>; rel=preload; as=fetch; crossorigin=use-credentials';
    $values = array_map(function (Link $link) use ($format) {
      $url = $link->getUri()->setAbsolute(FALSE)->toString(TRUE)->getGeneratedUrl();
      return sprintf($format, $url);
    }, $links);
    $response->headers->add(['Link' => $values]);
  }

}
